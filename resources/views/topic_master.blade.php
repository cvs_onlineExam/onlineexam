@extends('layout.layout')

	@section('topbar')
    	@parent
    		@include('shared.topnav')
	@endsection
    
    @section('sidebar')
		@parent
        @include('shared.sidenav')
    @endsection
    
    
	@section('content')
        
    
        
        
        	<div class="content-sec">
                    
                    
                    
                        
			<div class="breadcrumbs">
				<ul>
					<li><a href="dashboard.html" title=""><i class="fa fa-home"></i></a>/</li>
					<li><a title=""><b>Home Page</b></a></li>
				</ul>
			</div><!-- breadcrumbs -->
			<div class="container">
				
					<div class="row">
						     
                              
                                            <!---    Content --->
                                            
                                            
                                           
                                            
                                            
                                            
    
       <div class="divpadding">
    <a href="#" onclick="toggle_visibility('foo');">
     <button class='btn-md btn-primary' type='submit'> Add Topic </button></a>
        </div>

    <div id="foo" style="display:none">
    <div class='container'>
        <div class="col-md-12">
        <div class="widget-area">
            <h2 class="widget-title"><strong>Topic Master </strong></h2>
     
            <div class="wizard-form-h">
                <div id="Div1" class="swMain">
        <form class='form-horizontal' role='form'>
              <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='lblsubjectname'>Subject Name :</label>
            <div class='col-md-4'>
              <select class='form-control' id='ddlsubjectname'>
                   <option value="0">...Select...</option>
                <option>Engineering Chemistry</option>
                <option>Engineering Maths</option>
                <option>Engineering Physics</option>
              </select>
            </div>
          </div>

             <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='lbltopicname'>Topic Name :</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' id='txttopicname' placeholder='Topic-Name' type='text'>
                </div>
              </div>
             
            </div>
          </div>
            </form>
            <div class='form-group'>
            <div class='col-md-offset-4 col-md-3'>
              <button class='btn-md btn-primary' type='submit'>Save</button>
            </div>
          </div>
      </div>
         </div>
    </div></div></div>

     </div>

    <div class="container"> 
      <div class="col-md-12">
        <div class="widget-area">
            <h2 class="widget-title"><strong>Category</strong> list</h2>
            <div class="wizard-form-h">
                <div id="wizard" class="swMain">

                  <table class="table table-bordered">
								  <thead>
									<tr>
									  <th>Sr No</th>
									  <th>Subject Name</th>
									  <th>Topic Name</th>
                                      <th>Status</th>
									  <th>Update</th>
                                      <th>Delete</th>
									</tr>
								  </thead>
								  <tbody>
									<tr>
									  <td>1</td>
									  <td>Engineering Maths</td>
                                      <td>matrix</td>
									  <td>--</td>
									  <td><center><a href=""><img class="img_width15" src="../Theme/Images/Edit.png" /></a></center></td>
                                      <td><center><a href=""><img class="img_width15" src="../Theme/Images/Delete.png" /></a></center></td>
									</tr>
									<tr>
									  <td>2</td>
									  <td>Engineering Chemistry</td>
                                          <td>Grammer</td>
									  <td>--</td>
									  <td><center><a href=""><img class="img_width15" src="../Theme/Images/Edit.png" /></a></center></td>
                                      <td><center><a href=""><img class="img_width15" src="../Theme/Images/Delete.png" /></a></center></td>
									</tr>
                                      <tr>
									  <td>3</td>
									  <td>Engineering Physics</td>
                                            <td>Liquid</td>
									  <td>--</td>
									  <td><center><a href=""><img class="img_width15" src="../Theme/Images/Edit.png" /></a></center></td>
                                      <td><center><a href=""><img class="img_width15" src="../Theme/Images/Delete.png" /></a></center></td>
									</tr>
                                      <tr>
									  <td>4</td>
									  <td>Engineering Program</td>
                                            <td>Light</td>
									  <td>--</td>
									  <td><center><a href=""><img class="img_width15" src="../Theme/Images/Edit.png" /></a></center></td>
                                      <td><center><a href=""><img class="img_width15" src="../Theme/Images/Delete.png" /></a></center></td>
									</tr>
									 </tbody>
								</table>
                </div>
            </div>
        </div>
    </div>
        </div>
                                            
                                            
                                            <!-------  End Content   >--->
                          
					</div>
				
				
			</div>
		</div><!-- Content Sec -->
		
	</div><!-- Page Container -->
        

</div><!-- main -->


		
	@endsection
        
