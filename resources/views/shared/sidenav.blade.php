<div class="page-container menu-left">
<aside class="sidebar">
		
			
			<div class="menu-sec">
                
				<div id="menu-toogle" class="menus">
                                    
                                    
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-home"></i><b><span>Activities</b></span></a></h2>
						
					</div>
					<div class="single-menu">
						<h2><a href="{{url('topic')}}" title=""><i class="fa fa-calendar"></i><b><span>Topic MAster</span></b></a></h2>
						
					</div>
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-flask"></i><b><span>Schedule</span></b></a></h2>
					</div>
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-heart-o"></i><b><span>My Courses</span></b></a></h2>
						
					</div>
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-paperclip"></i><b><span>My Tutorial Meetings</span></b></a></h2>
					</div>
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-calendar-o"></i><b><span>Academic Calendar</span></b></a></h2>
					</div>
					<div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-desktop"></i><b><span>My Apps</span></b></a></h2>
						
					</div>
                    <div class="single-menu">
						<h2><a href="" title=""><i class="fa fa-file-text"></i><b><span>Smart Notes</span></b></a></h2>
						
					</div>
				</div>
				
			</div><!-- Menu Sec -->
		</aside><!-- Aside Sidebar -->
<script src="{{asset('js/jquery.min.js')}}"></script>
 