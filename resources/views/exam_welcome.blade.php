@extends('layout.exam_master')
	
    @section('content')

	@section('bread_scrumb', 'My Test >> Examination >> Welcome')
			<div class="container">				
				<div class="row">
					<div class="masonary-grids">
                    	<div class="col-md-12">
                         <!-- Start test -->
                         <form>
                         	<button class="btn green btn-primary" type="submit">Click Here To Start Test</button>
                         	<button class="btn green btn-primary" type="button">Review</button>
                         </form>
                         <!-- End Start test -->
                     </div>
                  </div>
              	</div>
           </div>
    @endsection       