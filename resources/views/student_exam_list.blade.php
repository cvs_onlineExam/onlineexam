@extends('layout.layout')

	@section('topbar')
    	@parent
    		@include('shared.topnav')
	@endsection
    
    @section('sidebar')
		@parent
        @include('shared.sidenav')
    @endsection
    
    
	@section('content')
        
    
        
        
        	<div class="content-sec">
                    
                    
                    
                        
			<div class="breadcrumbs">
				<ul>
					<li><a href="dashboard.html" title=""><i class="fa fa-home"></i></a>/</li>
					<li><a title=""><b>Home Page</b></a></li>
				</ul>
			</div><!-- breadcrumbs -->
			<div class="container">
				
					<div class="row">
						     
                              
                                            <!---    Content --->
                                            
                                            
                                            
                                            
                                            <div class="col-md-12">
        <div class="widget-area">
            <h2 class="widget-title"><strong>Student</strong> Information</h2>
            <div class="wizard-form-h">
                <div id="wizard1" class="swMain">

                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="boottableth">Admission No :</td>
                               
                                <td>120112E00013</td>
                                <td rowspan="4">
                                    <center>
                                    <img class="img100" src="Theme/Images/demoimage.jpg" />
                                        </center>
                                </td>
                            </tr>
                            <tr>
                                <td class="boottableth">Name :</td>
                               
                                <td>Sachin Mehar</td>

                            </tr>
                            <tr>
                                <td class="boottableth">Class :</td>
                                
                                <td>IT SEM-II</td>

                            </tr>
                            <tr>
                                <td class="boottableth">Roll No :</td>
                               
                                <td>ITSEM-001</td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-12">
        <div class="widget-area">
            <h2 class="widget-title"><strong>Practice</strong> Examination</h2>
            <div class="wizard-form-h">
                <div id="wizard" class="swMain">

                  <table class="table table-bordered">
								  <thead>
									<tr>
									  <th>Test Name</th>
									  <th>Duration</th>
									  <th>Number Of Attempt</th>
									  <th>Test Subjects</th>
                                      <th>Test Status</th>
									</tr>
								  </thead>
								  <tbody>
									<tr>
									  <td>Test 1</td>
									  <td>30</td>
									  <td>1</td>
									  <td>Maths</td>
                                      <td><center><button class="buttonradius"><b>give status</b></button></center></td>
									</tr>
									<tr>
									  <td>Test 2</td>
									  <td>30</td>
									  <td>2</td>
									  <td>Dotnet</td>
                                      <td>
                                          <center><button class="buttonradius"><b>give status</b></button></center></td>
									</tr>
									 </tbody>
								</table>
                </div>
            </div>
        </div>
    </div>
                                            
                                            
                                            
                                            <!-------  End Content   >--->
                          
					</div>
				
				
			</div>
		</div><!-- Content Sec -->
		
	</div><!-- Page Container -->
        

</div><!-- main -->


		
	@endsection
        
