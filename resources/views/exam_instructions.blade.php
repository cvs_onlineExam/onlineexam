@extends('layout.exam_master')
	
    @section('content')

	@section('bread_scrumb', 'My Test >> Examination >> Instructions')
			<div class="container">				
				<div class="row">
					<div class="masonary-grids">
                    <div class="col-md-12">
							<div class="widget-area">
								<h2 class="widget-title">Class</h2>
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>Category</th>
									  <th>Package Name</th>
									  <th>Test Code</th>
									  <th>Total Questions</th>
                                  <th>Total Time</th>
                                  <th>Maximum Marks</th>
									</tr>
								  </thead>
								  <tbody>
									<tr>
									  <td>Test 2</td>
									  <td></td>
									  <td>Practice</td>
									  <td>0</td>
                                  <td>30 min</td>
									  <td>20</td>    
									</tr>
									
								  </tbody>
								</table>
							</div>
						</div>
						<!-- instruction  -->
                        <div class="col-md-12">
							<div class="widget-area">
								<div class="typography">
									<h5>General Instruction: </small></h5>
									
									<p class="text-left">Left aligned text.</p>
									<p class="text-left">Please read the following instructions very carefully:</p>
                                <p class="text-left">1. You have 90 minutes to complete the test.</p>
                                <p class="text-left">2. The test contains a total of 50 questions .</p>
                                <p class="text-left">3. There is only one correct answer to each question. Click on the most appropriate option to mark it as your answer.</p>
                                <p class="text-left">4. You will be awarded one mark for each correct answer.</p>
                                <p class="text-left">5. There is 0 penalty mark.</p>
                                <p class="text-left">6. You can change your answer by clicking on some other option.</p>
                                <p class="text-left">7. You can move back and forth between the questions by clicking the buttons “Previous” and     “Next” respectively.</p>
                                <p class="text-left">8. A Number list of all questions appears at the right hand side of the screen. You can access the questions in any order within a section or across sections by clicking on the question number given on the number list.</p>
                                <p class="text-left">9. You can use rough sheets while taking the test. Do not use calculators, log tables, dictionaries, or any other printed/online reference material during the test.</p>
                                <p class="text-left">10. Do not click the button “End test” before completing the test. A test once submitted cannot be resumed.</p>
                                <p class="text-left">11.electonics gagets are not allowed in examination hall.</p>

								</div>
							</div>
						</div>
                     <!-- -->   
                     <!-- Start test -->
                     <div class="col-md-12">
							<div class="widget-area">
								<form role="form" class="sec" action="{{url('exam/welcome')}}">
								  
								  <div class="checkbox">
									<label>
									  <input type="checkbox" style="color:red"> I have read and understood the instructions. I agree that in case of not adhering to the exam instructions, I will be disqualified from giving the exam
									</label>
								  
                                  <button class="btn green pull-right" type="submit">Start Test</button>
                               </div>
								  
								</form>
							</div>
						</div>
                     <!-- End Start test -->
                  </div>
              	</div>
           </div>
    @endsection       