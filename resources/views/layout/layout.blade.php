
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
   
        
        





    <link href="Theme/Css/daterangepicker-bs3.css" rel="stylesheet" />
    <link href="Theme/Css/style.css" rel="stylesheet" />
    <link href="Theme/Css/rickshaw.css" rel="stylesheet" />
    <link href="Theme/Css/responsive.css" rel="stylesheet" />
    <link href="Theme/Css/jquery-jvectormap.css" rel="stylesheet" />
    <link href="Theme/Css/font-awesome.css" rel="stylesheet" />
    <link href="Theme/Css/bootstrap.css" rel="stylesheet" />



    
         
    <script src="Theme/Js/bootstrap.js"></script>
    <script src="Theme/Js/chart.js"></script>
    <script src="Theme/Js/d3.v2.js"></script>
    <script src="Theme/Js/daterangepicker.js"></script>
    <script src="Theme/Js/enscroll.js"></script>
    <script src="Theme/Js/extra.js"></script>
    <script src="Theme/Js/gdp-data.js"></script>
    <script src="Theme/Js/grid-filter.js"></script>
    <script src="Theme/Js/html5lightbox.js"></script>
    <script src="Theme/Js/jquery-1.11.1.js"></script>
    <script src="Theme/Js/jquery-jvectormap-world-en.js"></script>
    <script src="Theme/Js/jquery-jvectormap.js"></script>
    <script src="Theme/Js/jquery.sparkline.min.js"></script>
    <script src="Theme/Js/modernizr.js"></script>
    <script src="Theme/Js/moment.js"></script>
    <script src="Theme/Js/raphael-min.js"></script>
    <script src="Theme/Js/rickshaw.min.js"></script>
    <script src="Theme/Js/script.js"></script>



        
      <script type="text/javascript">

    function resettoggle() {
        var e = document.getElementById('foo');
        e.style.display = 'none';
    }

    function toggle_visibility(id) {
        var e = document.getElementById(id);
        if (e.style.display == 'none')
            e.style.display = 'block';
        else
            e.style.display = 'none';
    }
    //-->
</script>
    
    
    
    
    
    
    
    
    
    
    
    <script type="text/javascript">
        function random_num(field, interval, range) {

            setInterval(function () {
                var chars = "0123456789";
                var string_length = range;
                var randomstring = '';
                for (var i = 0; i < string_length; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    randomstring += chars.substring(rnum, rnum + 1);
                }
                var a = jQuery("#" + field).html(randomstring);
                console.log(a);
            }, interval);
        };
</script>
<script>
    jQuery(document).ready(function () {
        random_num('random', 3000, 3);
    });
</script>
<script>
    // set up our data series with 50 random data points

    var seriesData = [[], [], []];
    var random = new Rickshaw.Fixtures.RandomData(150);

    for (var i = 0; i < 100; i++) {
        random.addData(seriesData);
    }

    // instantiate our graph!

    var graph = new Rickshaw.Graph({
        element: document.getElementById("chart"),
        renderer: 'scatterplot',
        series: [
        {
            color: "#ffffff",
            data: seriesData[0],
        }, {
            color: "#eeeeee",
            data: seriesData[1],
        }
        ]
    });

    graph.renderer.dotSize = 3;

    new Rickshaw.Graph.HoverDetail({ graph: graph });

    graph.render();

</script>
<script type="text/javascript">
    $(document).ready(function () {
        var graph;

        var seriesData = [[], []];
        var random = new Rickshaw.Fixtures.RandomData(50);

        for (var i = 0; i < 50; i++) {
            random.addData(seriesData);
        }

        graph = new Rickshaw.Graph({
            element: document.querySelector("#serverload-chart"),
            height: 198,
            renderer: 'area',
            series: [
              {
                  data: seriesData[0],
                  color: '#b3b3b3',
                  name: 'File Server'
              }, {
                  data: seriesData[1],
                  color: '#505050',
                  name: 'Mail Server'
              }
            ]
        });

        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
        });

        setInterval(function () {
            random.removeData(seriesData);
            random.addData(seriesData);
            graph.update();

        }, 1000);


        $('#reportrange').daterangepicker(
        {
            startDate: moment().subtract('days', 29),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2014',
            dateLimit: { days: 60 },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        },
        function (start, end) {
            console.log("Callback has been called!");
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $(function () {
            $('#map').vectorMap({ map: 'world_en' });
        })


        $("#pie").sparkline([1, 1, 2], {
            type: 'pie',
            width: '40',
            height: '40',
            sliceColors: ['#2dcb73', '#fd6a59', '#17c3e5', '#109618', '#66aa00', '#dd4477', '#0099c6', '#990099 ']
        });

        $("#pie2").sparkline([2, 2, 2], {
            type: 'pie',
            width: '40',
            height: '40',
            sliceColors: ['#2dcb73', '#fd6a59', '#17c3e5', '#109618', '#66aa00', '#dd4477', '#0099c6', '#990099 ']
        });

        $("#pie3").sparkline([1, 1, 2, 3, 2], {
            type: 'pie',
            width: '40',
            height: '40',
            sliceColors: ['#2dcb73', '#fd6a59', '#17c3e5', '#109618', '#66aa00', '#dd4477', '#0099c6', '#990099 ']
        });

    });
</script>
    </head>
   
    
 	@section('topbar')
           
 	@show
 	 
 		@section('sidebar')
 
 		@show     
      
			
            @yield('content')
     
<!-- Script -->

  