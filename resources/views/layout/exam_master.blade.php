<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>@yield('title')</title>

<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />

<!-- Styles -->
<link rel="stylesheet" href="{{asset('font-awesome-4.2.0/css/font-awesome.css')}}" type="text/css" /><!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" /><!-- Style -->
<link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css" /><!-- Responsive -->	

</head>
<!-- <body style="background-image: url('images/resource/login-bg.jpg')">-->
<body style="">
<div class="breadcrumbs">
				<ul>
					<li><a href="dashboard.html" title=""><i class="fa fa-home"></i></a>/</li>
					<li><a title="">@yield('bread_scrumb')</a></li>
				</ul>
			</div><!-- breadcrumbs -->
           
           
@yield('content')
</body>
</html>