<?php /*@extends('layout.home_master')

@section('content')  
<div class="login-sec">
	<div class="login">
		<div class="login-form">
			<span><img src="images/logo.png" alt="" /></span>
			<h5><strong>Identify</strong> Yourself</h5>
            
            
			<form action="{{url('login')}}" method="post">
            	{!! csrf_field() !!}
                <p  style="color:#F00">
                	{{ $errors->first('Username') }}
                   {{ session('lgn_err') }}
            		</p>
				<fieldset>
                	
                	<input type="text" placeholder="Username" name="Username" id="Username" /><i class="fa fa-user"></i></fieldset>
                	<p  style="color:#F00">
                	{{ $errors->first('Password') }}
            		</p>
				<fieldset><input type="password" placeholder="Password" name="Password" id="Password" /><i class="fa fa-unlock-alt"></i></fieldset>
				<label><input type="checkbox" />Remember me</label><button type="submit" class="blue">LOG IN</button>
			</form>
			<a href="#" title="">Forgot Password?</a>
		</div>
		<ul class="reg-social-btns">
			<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
			<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
			<li><a href="#" title=""><i class="fa fa-github"></i></a></li>
			<li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
		</ul>
		<span>Copyright © 2014  CVS Pvt Ltd</span>
	</div>
</div><!-- Log in Sec -->	

@endsection
 * 
 * 
 */ ?>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <meta charset="UTF-8">
    <title>Login Form</title>
    <link href="Theme/Css/bootstrap.css" rel="stylesheet" /> 
    <link rel="stylesheet" href="Theme/Css/styless.css">
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
</head>
<body style="background-image:url(Theme/Images/college-backgrounds-10.jpg)">    
        
    <div class="container">
	<div class="row">
        <center>
		<form action="{{url('login')}}" method="post" class="form-signin mg-btm">
                    {!! csrf_field() !!}
                <p  style="color:#F00">
                	{{ $errors->first('Username') }}
                   {{ session('lgn_err') }}
            		</p>
    	<h2 class="heading-desc">
		
		CLOUD VISION SYSTEM</h2>
		<div class="social-box">
			<div class="row mg-btm">
             <div class="col-md-12">
                <a href="" class="btn btn-primary btn-block">
                  <i class="icon-facebook"></i>    Login with Facebook
                </a>
			</div>
			</div>
			<div class="row">
			<div class="col-md-3">
                <div class="social-popoutt"><img src="Theme/Images/Twiter.png" /></div>
               
            </div>
                <div class="col-md-3">
                <div class="social-popoutt"><img src="Theme/Images/pinterest.png" /></div>
               
            </div>
                <div class="col-md-3">
                <div class="social-popoutt"><img src="Theme/Images/googleplus.png" /></div>
               
            </div>
                <div class="col-md-3">
                <div class="social-popoutt"><img src="Theme/Images/linkedin.png" /></div>
               
            </div>
          </div>
		</div>
            
		<div class="main">	
        
                    <input type="text" placeholder="Username" name="Username" class="form-control"/>
            <br />
            <input type="password" placeholder="Password" name="Password"class="form-control"/>
		 <br />
        Are you admin? <a href=""> Get started here</a>
		<span class="clearfix"></span>	
        </div>
		<div class="login-footer">
		<div class="row">
                        <div class="col-xs-6 col-md-6">
                            <div class="left-section">
								<a href="">Forgot your password?</a>
								<a href="">Sign up now</a>
							</div>
                        </div>
                        <div class="col-xs-6 col-md-6 pull-right">
                            <button type="submit" class="btn btn-large btn-danger pull-right"><b>Login</b></button>
                        </div>
                    </div>
		
		</div>
      </form>
            </center>
	</div>
</div>



</body>
</html>