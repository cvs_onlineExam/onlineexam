<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// code 

//Session::forget('lgn_err');

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', 'exam@fn_index');

//Route::get("index","exam@fn_index");

Route::get("examlist","exam@fn_examlist");
Route::get("exam/instructions","exam@fn_examinstructions");
Route::get("exam/welcome","exam@fn_examwelcome");
Route::post("login","exam@fn_login");


Route::get("questbnk","question_bank@questionBank");

Route::get("quest-add","question_bank@manageQuestion");
Route::post("quest-add","question_bank@manageQuestion");

Route::get("topic","topic@manageTopic");
//Route::get("signout","question_bank@manageQuestion");



/// code vishal