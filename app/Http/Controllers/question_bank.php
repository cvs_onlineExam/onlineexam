<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

//use App\Http\Requests;

use Request;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;
use Session;

class question_bank extends Controller
{ 
    function questionBank(Request $request)
    {
        Session::forget('SuccessMsg');
        return view("question_bank");	
    }
    
    
    function test(){
        
    }
    
    
    function manageQuestion(Request $request)
    {
        $name = Request::get('submit');

        if ($name == 'Save'){              
            $rules = array('question'    => 'required');

            $validator = Validator::make(Request::all(), $rules);
            if ($validator->fails()) {
                
                
                return redirect()->back()->withErrors($validator);
                
		
            }else{
                $postData = Input::all();
                //insert data into mysql table
                 $data =      array('q_type'=> '33',
                                    'question'=> $postData['question'],
                                    'options' => $postData['option1'] 
                                   
                              );
                 
                  $ck = 0;
                  
                  
                $ck = DB::table('online_question_bank')->Insert($data);
                Session::put('SuccessMsg', "Record Added Successfully!"); 
                return view("question_bank");
                 

            }
            
            
        }else{
            return view("question_bank");	
        }
    }
    
    
	
	
}
