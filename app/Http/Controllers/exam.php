<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

//use App\Http\Requests;

use Request;
use DB;
use Validator;
use Session;

class exam extends Controller
{
    // index page
	
	
	function fn_index()
	{
		Session::forget('lgn_err');
		return view("index");	
	}
	
	function fn_examlist()
	{
            $admno = Session::get('admno'); 
            $data = DB::table('student')->leftJoin('class_sec', 'class_sec.id', '=', 'student.stu_class')->where('admno', $admno)->first();
            return view('student_exam_list')->with('data', $data);
        }
        
	function fn_examinstructions()
	{
		return view("exam_instructions");
	}
	function fn_examwelcome()
	{
		return view("exam_welcome");	
	}
	
	// login functions
	function fn_login(Request $request)
	{
				
			$rules = array(
			'Username'    => 'required', // make sure the email is an actual email
			'Password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
				);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Request::all(), $rules);
		
		//$request->session()->flash('status', 'Task was successful!');
		
		//Session::put('lgn_err', ""); //array index
		if ($validator->fails()) {
			return redirect()->intended("/")
				->withErrors($validator) // send back all errors to the login form
				->withInput(Request::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

			$name = Request::get('Username');
			$pwd= Request::get('Password');
			
			$exams = DB::table('users')
								->leftJoin('student', 'users.refid', '=', 'student.admno')
								->where('uniqueid', $name)							
								->where('password', md5($pwd))
						->get();
                       	
			//admno=refid
			Session::put('lgn_err', "Invalid UserName/Password"); //array index
			if($exams)
			{ 
                            foreach ($exams as $value) 
                                    $array['admno'] = $value->admno;
                            
                            
                            Session::put('admno', $array['admno']); 
                           
				$myerr = "Invalid User";
				return  redirect()->intended('examlist');
			}else{ 
				return view('index', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
			}
		}
	}
	
}
